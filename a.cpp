#include <cstdio>

namespace c {
namespace str {
static inline int len(const char *s);
static inline int cmp(const char *s, const char *t);
static inline void cpy(char *s, const char *t);
static inline void cat(char *s, const char *t);
}; // namespace str
} // namespace c

int c::str::len(const char *s) {
  int n = 0;

  while (*s++)
    ++n;

  return n;
}

int c::str::cmp(const char *s, const char *t) {
  for (; *s == *t; ++s, ++t)
    if (!*s)
      return 0;

  return *s - *t;
}

void c::str::cpy(char *s, const char *t) {
  while ((*s++ = *t++))
    ;
}

void c::str::cat(char *s, const char *t) {
  while (*s)
    ++s;
  c::str::cpy(s, t);
}

int main() {
  std::printf("%d\n", c::str::len("Hello"));
  std::printf("%d\n", c::str::cmp("Hello", "Hellr"));
  char str[13] = "Hello there!";
  c::str::cpy(str, "Hi");
  std::printf("%s\n", str);
  c::str::cat(str, " there!");
  std::printf("%s\n", str);
}
